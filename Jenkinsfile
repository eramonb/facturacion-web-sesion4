﻿def IP_SERVIDOR = "192.168.1.68"
def URL_GIT_REPOSITORIO = "https://gitlab.com/eramonb/facturacion-web-sesion4.git"

pipeline {

    agent any
    
	environment {
    	ENV_FACTURACION_WEB_BD_TEST_URL = "jdbc:mysql://${IP_SERVIDOR}:3306/facturacion-web?useSSL=false"
   	}
   
    stages {
        stage('Descargar fuentes') {
           steps {
               echo 'Descargando fuentes...'
				git credentialsId: 'eramonb', url: "${URL_GIT_REPOSITORIO}"
           }
        }
        
        stage('Compilar') {
           steps {
               echo 'Compilando...'
               sh 'mvn -f /fuentes/pom.xml clean compile'
           }
        }
        
        stage('Pruebas Unitarias') {
        	steps {
				echo 'Ejecutando pruebas unitarias...'
				sh 'mvn -f fuentes/fuentes/pom.xml test'
				junit '**/target/surefire-reports/*.xml'
	        }
		}
		
		stage('Pruebas Integración') {
			steps {
				echo 'Ejecutando pruebas integración...'
				sh "mysql --host=${IP_SERVIDOR} --user=root --password=admin123 -e \"source fuentes/src/main/resources/db/create_database_integration_test_mysql.sql\""
				sh 'mvn -f fuentes/fuentes/pom.xml verify -DskipUTs=true -Dspring.profiles.active=test'
				sh "mysql --host=${IP_SERVIDOR} --user=root --password=admin123 -e \"source fuentes/src/main/resources/db/delete_database_integration_test_mysql.sql\""
				junit '**/target/failsafe-reports/*.xml'
			}
		}
		
		stage('Análisis Estático con SonarQube') {
			steps {
			    echo 'Análisis Estático con SonarQube...'
			
				withSonarQubeEnv('sonarqube-curso-devops') {
					sh "mvn -f fuentes/fuentes/pom.xml sonar:sonar -Dsonar.host.url=http://${IP_SERVIDOR}:9000"
				}
				
			}
			
		}
		
		stage('Build') {
			steps {
				echo 'Generando build...'
				sh 'mvn -f fuentes/fuentes/pom.xml package -DskipTests=true'
			}
		}
	
       	stage('Versionar Artifactory') {
			steps {
				echo 'Versionando Artifactory...'
				
				rtServer (
				    id: 'artifactory-curso-devops',
				    url: "http://${IP_SERVIDOR}:8081/artifactory",
				    credentialsId: 'caltamiranob'
			  )
			  
			  rtUpload (
			    serverId: 'artifactory-curso-devops',
			    spec: '''{
			          "files": [
			            {
			              "pattern": "fuentes/target/*.jar",
			              "target": "facturacion-web/desarrollo/${BUILD_NUMBER}/",
			              "props": "pruebas-unitarias=si;pruebas-integracion=si;analisis-codigo-estatico=si"
			            }
			         ]
			    }'''
			  )
			}
		} 
    }
}
